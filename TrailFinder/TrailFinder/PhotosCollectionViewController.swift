//
//  PhotosCollectionViewController.swift
//  TrailFinder
//
//  Created by Matthew Brown on 12/7/17.
//  Copyright © 2017 Matthew Brown. All rights reserved.
//

import UIKit
import Firebase

private let reuseIdentifier = "Cell"

class PhotosCollectionViewController: UICollectionViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var selectedTrail: BikeTrail!
    var trailPhotos = [UIImage]()
    let storageRef = AppDelegate.shared().storageRef
    let imagePicker = UIImagePickerController()
    let ref = AppDelegate.shared().ref

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        imagePicker.delegate = self
        getTrailPhotos()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getTrailPhotos() {
        trailPhotos.removeAll()
        /*ref?.child("Images").child(selectedTrail.uniqueKey).observeSingleEvent(of: .value) { (snapshot) in
            let value = snapshot.value as? [String: Any]
            if value == nil {
                return
            }
            let keyCount = value?.keys.count
            var counter = 0
            for key in (value?.keys)! {
                self.storageRef?.child("/trail_photos/\(self.selectedTrail.uniqueKey)/\(key).jpg").getData(maxSize: 1 * 4000 * 4000, completion: { (data, error) in
                    if data != nil {
                        print("data is not nil")
                        let pic = UIImage(data: data!)
                        self.trailPhotos.append(pic!)
                        counter = counter + 1
                        if counter == keyCount {
                            self.collectionView?.reloadData()
                        }
                    }
                })
                print(key)
            }
        }*/
        let trailID = self.selectedTrail.uniqueKey
        let getURL = URL(string: "http://127.0.0.1:3000/Photos?trailid=\(trailID)")!
        var getRequest = URLRequest(url: getURL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 30.0)
        getRequest.httpMethod = "GET"
        getRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        getRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        
        URLSession.shared.dataTask(with: getRequest, completionHandler: { (data, response, error) -> Void in
            if error != nil { print("GET Request: Communication error: \(error!)") }
            if data != nil {
                do {
                    let resultObject = try JSONSerialization.jsonObject(with: data!, options: [])
                    DispatchQueue.main.async(execute: {
                        print("Results from GET https://httpbin.org/get?bar=foo :\n\(resultObject)")
                        for item in (resultObject as! NSArray) {
                            let itemDict = (item as! NSDictionary)
                            let key = itemDict["Path"] as! String
                            self.storageRef?.child(key).getData(maxSize: 1 * 4000 * 4000, completion: { (data, error) in
                                if data != nil {
                                    print("data is not nil")
                                    let pic = UIImage(data: data!)
                                    self.trailPhotos.append(pic!)
                                    self.collectionView?.reloadData()
                                }
                            })
                            print(key)
                        }
                    })
                } catch {
                    DispatchQueue.main.async(execute: {
                        print("Unable to parse JSON response")
                    })
                }
            } else {
                DispatchQueue.main.async(execute: {
                    print("Received empty response.")
                })
            }
        }).resume()
    }

    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return trailPhotos.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoCell", for: indexPath) as! PhotosCollectionViewCell
    
        // Configure the cell
        cell.imageView.image = trailPhotos[indexPath.row]
    
        return cell
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let photo = info[UIImagePickerControllerOriginalImage] as? UIImage, let photoData = UIImageJPEGRepresentation(photo, 0.8) {
            let metadata = StorageMetadata()
            metadata.contentType = "image/jpeg"
            let pictureID = UUID().uuidString
            let filePath = "/trail_photos/\(selectedTrail.uniqueKey)/\(pictureID).jpg"
            storageRef?.child(filePath).putData(photoData, metadata: metadata) {
                (metadata, error) in
                if let error = error {
                    print("error uploading: \(error)")
                    return
                }
                else {
                    //self.ref?.child("Images").child(self.selectedTrail.uniqueKey).child(pictureID).setValue(1)
                    let trailID = self.selectedTrail.uniqueKey
                    let postURL = URL(string: "http://127.0.0.1:3000/Photos?trailid=\(trailID)")!
                    var postRequest = URLRequest(url: postURL, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 60.0)
                    postRequest.httpMethod = "POST"
                    postRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
                    postRequest.setValue("application/json", forHTTPHeaderField: "Accept")
                    
                    let parameters: [String: Any] = ["path": filePath, "trailid": trailID]
                    do {
                        let jsonParams = try JSONSerialization.data(withJSONObject: parameters, options: [])
                        postRequest.httpBody = jsonParams
                    } catch { print("Error: unable to add parameters to POST request.")}
                    
                    URLSession.shared.dataTask(with: postRequest, completionHandler: { (data, response, error) -> Void in
                        if error != nil { print("POST Request: Communication error: \(error!)") }
                        if data != nil {
                            do {
                                let resultObject = try JSONSerialization.jsonObject(with: data!, options: [])
                                DispatchQueue.main.async(execute: {
                                    print("Results from POST https://httpbin.org/post :\n\(resultObject)")
                                    self.getTrailPhotos()
                                })
                            } catch {
                                DispatchQueue.main.async(execute: {
                                    print("Unable to parse JSON response")
                                    self.getTrailPhotos()
                                })
                            }
                        } else {
                            DispatchQueue.main.async(execute: {
                                print("Received empty response.")
                                self.getTrailPhotos()
                            })
                        }
                    }).resume()
                }
            }
        }
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
