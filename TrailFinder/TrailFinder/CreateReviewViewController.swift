//
//  CreateReviewViewController.swift
//  TrailFinder
//
//  Created by Matthew Brown on 12/4/17.
//  Copyright © 2017 Matthew Brown. All rights reserved.
//

import UIKit
import Firebase

class CreateReviewViewController: UIViewController {
    
    @IBOutlet weak var trailLabel: UILabel!
    @IBOutlet weak var ratingOutlet: RatingControl!
    @IBOutlet weak var textViewOutlet: UITextView!
    
    var selectedTrail: BikeTrail!
    let ref = AppDelegate.shared().ref
    let apiString = "http://127.0.0.1:3000/Reviews"

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func cancelButtonPressed(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func submitButtonPressed(_ sender: UIBarButtonItem) {
        let userName = Auth.auth().currentUser?.displayName
        let userID = Auth.auth().currentUser?.uid
        let reviewText = textViewOutlet.text
        let rating = ratingOutlet.rating
        let formatter = DateFormatter()
        //formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //let myString = formatter.string(from: Date())
        //let yourDate = formatter.date(from: myString)
        formatter.dateFormat = "MMM dd,yyyy"
        //let myStringafd = formatter.string(from: yourDate!)
        
        /*let reviewRef = ref?.child("Reviews").child(selectedTrail.uniqueKey).childByAutoId()
        reviewRef?.child("userName").setValue(userName)
        reviewRef?.child("userID").setValue(userID)
        reviewRef?.child("reviewText").setValue(reviewText)
        reviewRef?.child("rating").setValue(rating)
        reviewRef?.child("date").setValue(myStringafd)*/
        
        let postURL = URL(string: apiString)!
        var postRequest = URLRequest(url: postURL, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 60.0)
        postRequest.httpMethod = "POST"
        postRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        postRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        
        let parameters: [String: Any] = ["username": userName!, "userid": userID!, "text": reviewText!, "rating": rating, "trailid": selectedTrail.uniqueKey]
        do {
            let jsonParams = try JSONSerialization.data(withJSONObject: parameters, options: [])
            postRequest.httpBody = jsonParams
        } catch { print("Error: unable to add parameters to POST request.")}
        
        URLSession.shared.dataTask(with: postRequest, completionHandler: { (data, response, error) -> Void in
            if error != nil { print("POST Request: Communication error: \(error!)") }
            if data != nil {
                do {
                    let resultObject = try JSONSerialization.jsonObject(with: data!, options: [])
                    DispatchQueue.main.async(execute: {
                        print("Results from POST https://httpbin.org/post :\n\(resultObject)")
                    })
                } catch {
                    DispatchQueue.main.async(execute: {
                        print("Unable to parse JSON response")
                        let stack = self.navigationController?.viewControllers
                        let previousView = stack?[(stack?.count)! - 2] as! TrailDetailTableViewController
                        previousView.tableView.reloadData()
                        previousView.viewDidLoad()
                        self.navigationController?.popViewController(animated: true)
                    })
                }
            } else {
                DispatchQueue.main.async(execute: {
                    print("Received empty response.")
                    
                })
            }
        }).resume()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
