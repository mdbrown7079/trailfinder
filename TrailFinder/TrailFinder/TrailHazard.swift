//
//  TrailHazard.swift
//  TrailFinder
//
//  Created by Matthew Brown on 12/2/17.
//  Copyright © 2017 Matthew Brown. All rights reserved.
//

import Foundation
import CoreLocation
import Mapbox

class TrailHazard: NSObject, MGLAnnotation {
    
    var title: String?
    var coordinate: CLLocationCoordinate2D
    var startTime: Date
    var endTime: Date
    
    init(description: String, location: CLLocationCoordinate2D, startTime: Date, endTime: Date) {
        self.title = description
        self.coordinate = location
        self.startTime = startTime
        self.endTime = endTime
    }
}
