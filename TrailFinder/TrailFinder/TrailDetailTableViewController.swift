//
//  TrailDetailTableViewController.swift
//  TrailFinder
//
//  Created by Matthew Brown on 12/5/17.
//  Copyright © 2017 Matthew Brown. All rights reserved.
//

import UIKit
import Firebase
import MapKit
import CoreLocation
import Foundation

class TrailDetailTableViewController: UITableViewController {
    
    var selectedTrail: BikeTrail!
    var selectedTrailReviews = [TrailReview]()
    var savedTrailIds = Set<String>()
    let ref = Database.database().reference()
    let locationManager = CLLocationManager()
    let isoDateFormatter = ISO8601DateFormatter()
    
    @IBOutlet weak var ratingOutlet: RatingControl!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var difficultyLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var ascentLabel: UILabel!
    @IBOutlet weak var descentLabel: UILabel!
    @IBOutlet weak var minGradeLabel: UILabel!
    @IBOutlet weak var maxGradeLabel: UILabel!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLabel.text = selectedTrail.name
        cityLabel.text = "\(selectedTrail.city.capitalized), CA"
        typeLabel.text = selectedTrail.type
        difficultyLabel.text = selectedTrail.difficulty
        distanceLabel.text = "\(selectedTrail.distance) Miles"
        ascentLabel.text = "\(selectedTrail.ascent)"
        descentLabel.text = "\(selectedTrail.descent)"
        minGradeLabel.text = "\(selectedTrail.minGrade)%"
        maxGradeLabel.text = "\(selectedTrail.maxGrade)%"
        getReviews()
        getFavoriteTrailIds()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return selectedTrailReviews.count
    }
    
    func getReviews() {
        selectedTrailReviews.removeAll()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let trailID = selectedTrail.uniqueKey
        let getURL = URL(string: "http://127.0.0.1:3000/Reviews?trailid=\(trailID)")!
        var getRequest = URLRequest(url: getURL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 30.0)
        getRequest.httpMethod = "GET"
        getRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        getRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        
        URLSession.shared.dataTask(with: getRequest, completionHandler: { (data, response, error) -> Void in
            if error != nil { print("GET Request: Communication error: \(error!)") }
            if data != nil {
                do {
                    let resultObject = try JSONSerialization.jsonObject(with: data!, options: [])
                    DispatchQueue.main.async(execute: {
                        print("Results from GET https://httpbin.org/get?bar=foo :\n\(resultObject)")
                        for item in (resultObject as! NSArray) {
                            let review = (item as! NSDictionary)
                            print(review["Trailid"] as! String)
                            let isoDateString = review["Date"] as! String
                            let trimmedIsoString = isoDateString.replacingOccurrences(of: "\\.\\d+", with: "", options: .regularExpression)
                            print(trimmedIsoString)
                            var trimmedIsoStringArr = trimmedIsoString.components(separatedBy: " ")
                            let swiftDateString = trimmedIsoStringArr[0] + " " + trimmedIsoStringArr[1]
                    
                            self.selectedTrailReviews.append(TrailReview(userName: review["Username"] as! String, userID: review["Userid"] as! String, text: review["Text"] as! String, rating: review["Rating"] as! Int, date: dateFormatter.date(from: swiftDateString)!))
                        }
                        self.tableView.reloadData()
                    })
                    
                } catch {
                    DispatchQueue.main.async(execute: {
                        print("Unable to parse JSON response")
                    })
                }
            } else {
                DispatchQueue.main.async(execute: {
                    print("Received empty response.")
                })
            }
        }).resume()
        /*ref.child("Reviews").child(selectedTrail.uniqueKey).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? [String: Any]
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd,yyyy"
            if value == nil {
                return
            }
            for key in (value?.keys)! {
                let review = value![key] as? [String: Any]
                let currentReview = TrailReview(userName: review!["userName"] as! String, userID: review!["userID"] as! String, text: review!["reviewText"] as! String, rating: review!["rating"] as! Int, date: dateFormatter.date(from: review!["date"] as! String)!)
                self.selectedTrailReviews.append(currentReview)
            }
            self.tableView.reloadData()
        }) { (error) in
            print(error.localizedDescription)
        }*/
        /*selectedTrailReviews.append(TrailReview(userName: "Matt Brown", userID: (Auth.auth().currentUser?.uid)!, text: "This is a short review.", rating: 5, date: Date()))
        selectedTrailReviews.append(TrailReview(userName: "Matt Brown", userID: (Auth.auth().currentUser?.uid)!, text: "This is a much much much much much much much much much much much much much much much much much much much much much much much much much much much much much much much much much longer review.", rating: 3, date: Date()))*/
    }
    
    func getFavoriteTrailIds() {
        /*ref.child("Saved").child((Auth.auth().currentUser?.uid)!).observeSingleEvent(of: .value, with: ({ (snapshot) in
            let value = snapshot.value as? [String: Any]
            if value == nil {
                return
            }
            for key in (value?.keys)! {
                self.savedTrailIds.insert(key)
            }
            print("saved trail ids: \(self.savedTrailIds.count)")
            if self.savedTrailIds.contains(self.selectedTrail.uniqueKey) {
                self.saveButton.image = UIImage(named: "pdfFilledHeart")
            }
        }))*/
        let userID = Auth.auth().currentUser?.uid
        let getURL = URL(string: "http://127.0.0.1:3000/Favorites?userid=\(userID!)")!
        var getRequest = URLRequest(url: getURL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 30.0)
        getRequest.httpMethod = "GET"
        getRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        getRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        
        URLSession.shared.dataTask(with: getRequest, completionHandler: { (data, response, error) -> Void in
            if error != nil { print("GET Request: Communication error: \(error!)") }
            if data != nil {
                do {
                    let resultObject = try JSONSerialization.jsonObject(with: data!, options: [])
                    DispatchQueue.main.async(execute: {
                        print("Results from GET https://httpbin.org/get?bar=foo :\n\(resultObject)")
                        for item in (resultObject as! NSArray) {
                            let trail = (item as! NSDictionary)
                            print(trail["ID"] as! String)
                            
                            self.savedTrailIds.insert(trail["ID"] as! String)
                        }
                        if self.savedTrailIds.contains(self.selectedTrail.uniqueKey) {
                            self.saveButton.image = UIImage(named: "pdfFilledHeart")
                        }
                    })
                    
                } catch {
                    DispatchQueue.main.async(execute: {
                        print("Unable to parse JSON response")
                    })
                }
            } else {
                DispatchQueue.main.async(execute: {
                    print("Received empty response.")
                })
            }
        }).resume()
    }

    @IBAction func reviewButtonPressed(_ sender: UIButton) {
        performSegue(withIdentifier: "reviewSegue", sender: self)
    }
    
    @IBAction func photosButtonPressed(_ sender: UIButton) {
        performSegue(withIdentifier: "photoSegue", sender: self)
    }
    
    @IBAction func directionsButtonPressed(_ sender: UIButton) {
        let trailPlacemark = MKPlacemark(coordinate: selectedTrail.entrance)
        let myPlacemark = MKPlacemark(coordinate: (locationManager.location?.coordinate)!)
        let trailMapItem = MKMapItem(placemark: trailPlacemark)
        let myMapItem = MKMapItem(placemark: myPlacemark)
        
        let mapItems = [trailMapItem, myMapItem]
        
        let directionOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        MKMapItem.openMaps(with: mapItems, launchOptions: directionOptions)
    }
    
    @IBAction func saveButtonPressed(_ sender: UIBarButtonItem) {
        let userID = Auth.auth().currentUser?.uid
        if !self.savedTrailIds.contains(selectedTrail.uniqueKey) {
            self.savedTrailIds.insert(self.selectedTrail.uniqueKey)
            self.saveButton.image = UIImage(named: "pdfFilledHeart")
            
            /*self.ref.child("Saved").child((Auth.auth().currentUser?.uid)!).child(self.selectedTrail.uniqueKey).setValue(1)*/
            let postURL = URL(string: "http://127.0.0.1:3000/Favorites")!
            var postRequest = URLRequest(url: postURL, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 60.0)
            postRequest.httpMethod = "POST"
            postRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            postRequest.setValue("application/json", forHTTPHeaderField: "Accept")
            
            let parameters: [String: Any] = ["userid": userID!, "trailid": selectedTrail.uniqueKey]
            do {
                let jsonParams = try JSONSerialization.data(withJSONObject: parameters, options: [])
                postRequest.httpBody = jsonParams
            } catch { print("Error: unable to add parameters to POST request.")}
            
            URLSession.shared.dataTask(with: postRequest, completionHandler: { (data, response, error) -> Void in
                if error != nil { print("POST Request: Communication error: \(error!)") }
                if data != nil {
                    do {
                        let resultObject = try JSONSerialization.jsonObject(with: data!, options: [])
                        DispatchQueue.main.async(execute: {
                            print("Results from POST https://httpbin.org/post :\n\(resultObject)")
                            let tabBar = self.tabBarController?.viewControllers
                            if tabBar != nil {
                                print("tab bar not nil")
                            }
                            let favoriteNavCon = tabBar![2] as? UINavigationController
                            if favoriteNavCon != nil {
                                let stack = favoriteNavCon?.viewControllers
                                if stack != nil {
                                    let favoritesVC = stack![0] as? FavoritesCollectionViewController
                                    if favoritesVC != nil {
                                        favoritesVC?.getAllFavoriteTrails()
                                        //favoritesVC?.collectionView?.reloadData()
                                    }
                                }
                            }
                        })
                    } catch {
                        DispatchQueue.main.async(execute: {
                            print("Unable to parse JSON response")
                            let tabBar = self.tabBarController?.viewControllers
                            if tabBar != nil {
                                print("tab bar not nil")
                            }
                            let favoriteNavCon = tabBar![2] as? UINavigationController
                            if favoriteNavCon != nil {
                                let stack = favoriteNavCon?.viewControllers
                                if stack != nil {
                                    let favoritesVC = stack![0] as? FavoritesCollectionViewController
                                    if favoritesVC != nil {
                                        favoritesVC?.getAllFavoriteTrails()
                                        //favoritesVC?.collectionView?.reloadData()
                                    }
                                }
                            }
                        })
                    }
                } else {
                    DispatchQueue.main.async(execute: {
                        print("Received empty response.")
                        print("Hello")
                        let tabBar = self.tabBarController?.viewControllers
                        if tabBar != nil {
                            print("tab bar not nil")
                        }
                        let favoriteNavCon = tabBar![2] as? UINavigationController
                        if favoriteNavCon != nil {
                            let stack = favoriteNavCon?.viewControllers
                            if stack != nil {
                                let favoritesVC = stack![0] as? FavoritesCollectionViewController
                                if favoritesVC != nil {
                                    favoritesVC?.getAllFavoriteTrails()
                                    //favoritesVC?.collectionView?.reloadData()
                                }
                            }
                        }
                    })
                }
            }).resume()
            
        }
        else {
            self.savedTrailIds.remove(self.selectedTrail.uniqueKey)
            self.saveButton.image = UIImage(named: "pdfUnfilledHeart")
            
            //self.ref.child("Saved").child((Auth.auth().currentUser?.uid)!).child(self.selectedTrail.uniqueKey).removeValue()
            let deleteURL = URL(string: "http://127.0.0.1:3000/Favorites")!
            var deleteRequest = URLRequest(url: deleteURL, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 60.0)
            deleteRequest.httpMethod = "DELETE"
            deleteRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            deleteRequest.setValue("application/json", forHTTPHeaderField: "Accept")
            
            let parameters: [String: Any] = ["userid": userID!, "trailid": selectedTrail.uniqueKey]
            do {
                let jsonParams = try JSONSerialization.data(withJSONObject: parameters, options: [])
                deleteRequest.httpBody = jsonParams
            } catch { print("Error: unable to add parameters to POST request.")}
            
            URLSession.shared.dataTask(with: deleteRequest, completionHandler: { (data, response, error) -> Void in
                if error != nil { print("DELETE Request: Communication error: \(error!)") }
                if data != nil {
                    do {
                        let resultObject = try JSONSerialization.jsonObject(with: data!, options: [])
                        DispatchQueue.main.async(execute: {
                            print("Results from DELETE https://httpbin.org/post :\n\(resultObject)")
                            let tabBar = self.tabBarController?.viewControllers
                            if tabBar != nil {
                                print("tab bar not nil")
                            }
                            let favoriteNavCon = tabBar![2] as? UINavigationController
                            if favoriteNavCon != nil {
                                let stack = favoriteNavCon?.viewControllers
                                if stack != nil {
                                    let favoritesVC = stack![0] as? FavoritesCollectionViewController
                                    if favoritesVC != nil {
                                        favoritesVC?.getAllFavoriteTrails()
                                        favoritesVC?.collectionView?.reloadData()
                                    }
                                }
                            }
                        })
                    } catch {
                        DispatchQueue.main.async(execute: {
                            print("Unable to parse JSON response")
                            let tabBar = self.tabBarController?.viewControllers
                            if tabBar != nil {
                                print("tab bar not nil")
                            }
                            let favoriteNavCon = tabBar![2] as? UINavigationController
                            if favoriteNavCon != nil {
                                let stack = favoriteNavCon?.viewControllers
                                if stack != nil {
                                    let favoritesVC = stack![0] as? FavoritesCollectionViewController
                                    if favoritesVC != nil {
                                        favoritesVC?.getAllFavoriteTrails()
                                        favoritesVC?.collectionView?.reloadData()
                                    }
                                }
                            }
                            
                        })
                    }
                } else {
                    DispatchQueue.main.async(execute: {
                        print("Received empty response.")
                        print("Hello")
                        let tabBar = self.tabBarController?.viewControllers
                        if tabBar != nil {
                            print("tab bar not nil")
                        }
                        let favoriteNavCon = tabBar![2] as? UINavigationController
                        if favoriteNavCon != nil {
                            let stack = favoriteNavCon?.viewControllers
                            if stack != nil {
                                let favoritesVC = stack![0] as? FavoritesCollectionViewController
                                if favoritesVC != nil {
                                    favoritesVC?.getAllFavoriteTrails()
                                    favoritesVC?.collectionView?.reloadData()
                                }
                            }
                        }
                    })
                }
            }).resume()
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reviewCell", for: indexPath) as! ReviewTableViewCell

        // Configure the cell...
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd,yyyy"
        cell.nameLabel.text = selectedTrailReviews[indexPath.row].userName
        cell.ratingOutlet.rating = selectedTrailReviews[indexPath.row].rating
        cell.dateLabel.text = dateFormatter.string(from: selectedTrailReviews[indexPath.row].date)
        cell.reviewLabel.text = selectedTrailReviews[indexPath.row].text

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "reviewSegue" {
            let destVC = segue.destination as? CreateReviewViewController
            destVC?.selectedTrail = self.selectedTrail
        }
        if segue.identifier == "photoSegue" {
            let photoVC = segue.destination as? PhotosCollectionViewController
            photoVC?.selectedTrail = self.selectedTrail
        }
    }

}
