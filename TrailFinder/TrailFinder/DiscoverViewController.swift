//
//  SecondViewController.swift
//  TrailFinder
//
//  Created by Matthew Brown on 12/2/17.
//  Copyright © 2017 Matthew Brown. All rights reserved.
//

import UIKit
import CoreLocation
import Mapbox
import Firebase

class DiscoverViewController: UIViewController {
    
    var allTrails = [BikeTrail]()
    var bufferedTrails = [BikeTrail]()
    var nearbyTrails = [BikeTrail]()
    var trailsByDifficulty = [BikeTrail]()
    let locationManager = CLLocationManager()
    let ref = Database.database().reference()
    var currentDistance = 10
    var mapChildVC: DiscoverMapViewController?
    var tableChildVC: DiscoverTableViewController?
    var selectedTrail: BikeTrail?
    var lastLocation: CLLocation? = nil
    var sortByDistance = true
    
    let apiString = "http://127.0.0.1:3000/Trails"
    
    @IBOutlet weak var segmentOutlet: UISegmentedControl!
    @IBOutlet weak var resultsLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //self.allTrails = AppDelegate.shared().allTrails
        //AppDelegate.shared().discoverVC = self
        //getAllTrails()
        getTrailsFromAPI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func getTrailsFromAPI() {
        self.nearbyTrails = []
        self.trailsByDifficulty = []
        let loc = locationManager.location
        let lat = (loc?.coordinate.latitude)!
        let lon = (loc?.coordinate.longitude)!
        let newString = "\(apiString)?lat=\(lat)&lon=\(lon)&distance=\(currentDistance)"
        let apiURL = URL(string: newString)!
        var getRequest = URLRequest(url: apiURL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 30.0)
        getRequest.httpMethod = "GET"
        getRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        getRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        print(newString)
        
        URLSession.shared.dataTask(with: getRequest, completionHandler: { (data, response, error) -> Void in
            if error != nil { print("GET Request: Communication error: \(error!)") }
            if data != nil {
                do {
                    let resultObject = try JSONSerialization.jsonObject(with: data!, options: [])
                    DispatchQueue.main.async(execute: {
                        //print("Results from GET https://httpbin.org/get?bar=foo :\n\(resultObject)")
                        for item in (resultObject as! NSArray) {
                            let trail = (item as! NSDictionary)
                            print(trail["Name"] as! String)
                            self.nearbyTrails.append(BikeTrail(name: trail["Name"] as! String, park: trail["Park"] as! String, city: trail["City"] as! String, state: trail["State"] as! String, difficulty: trail["Difficulty"] as! String ,type: trail["Type"] as! String, distance: trail["Distance"] as! Double, ascent: trail["Ascent"] as! Double, descent: trail["Descent"] as! Double, high: trail["High"] as! Double, low: trail["Low"] as! Double, minGrade: trail["Mingrade"] as! Double, maxGrade: trail["Maxgrade"] as! Double, entrance: CLLocationCoordinate2D(latitude: trail["Lat"] as! Double, longitude: trail["Lon"] as! Double), uniqueKey: trail["ID"] as! String))
                        }
                       // self.tableChildVC?.nearbyTrails = self.nearbyTrails
                        //self.tableChildVC?.viewDidLoad()
                        self.mapChildVC?.mapView.removeAnnotations((self.mapChildVC?.nearbyTrailAnnotations)!)
                        self.mapChildVC?.nearbyTrailAnnotations.removeAll()
                        for trail in self.nearbyTrails {
                            let nextAnnotation = TrailAnnotation(coordinate: trail.entrance)
                            self.mapChildVC?.nearbyTrailAnnotations.append(nextAnnotation)
                            self.trailsByDifficulty.append(trail)
                        }
                        self.trailsByDifficulty.sort{ (loc1, loc2) -> Bool in
                            self.myDifficultySortFunc(trail1: loc1, trail2: loc2)
                        }
                        if self.sortByDistance {
                            self.tableChildVC?.nearbyTrails = self.nearbyTrails
                        } else {
                            self.tableChildVC?.nearbyTrails = self.trailsByDifficulty
                        }
                        self.mapChildVC?.currentDistance = self.currentDistance
                        self.mapChildVC?.viewDidLoad()
                        self.tableChildVC?.viewDidLoad()
                        self.resultsLabel.text = "\(self.nearbyTrails.count) RESULTS"
                    })
                } catch {
                    DispatchQueue.main.async(execute: {
                        print("Unable to parse JSON response")
                    })
                }
            } else {
                DispatchQueue.main.async(execute: {
                    print("Received empty response.")
                })
            }
        }).resume()
    }
    
    /*func getAllTrails() {
        ref.child("Trails").observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? [String: Any]
            for key in (value?.keys)! {
                var trail = value![key] as! [String:Any]
                self.allTrails.append(BikeTrail(name: trail["name"] as! String, park: trail["park"] as! String, city: trail["city"] as! String, state: trail["state"] as! String, difficulty: trail["difficulty"] as! String ,type: trail["type"] as! String, distance: trail["distance"] as! Double, ascent: trail["ascent"] as! Double, descent: trail["descent"] as! Double, high: trail["high"] as! Double, low: trail["low"] as! Double, minGrade: trail["minGrade"] as! Double, maxGrade: trail["maxGrade"] as! Double, entrance: CLLocationCoordinate2D(latitude: trail["lat"] as! Double, longitude: trail["lon"] as! Double), uniqueKey: key))
            }
            //print(self.allTrails.count)
            self.getNearbyTrails()
            //self.mapChildVC?.viewDidLoad()
            //self.tableChildVC?.viewDidLoad()
        }) { (error) in
            print(error.localizedDescription)
        }
    }*/
    
    /*func getNearbyTrails() {
        if lastLocation == nil || locationManager.location!.distance(from: lastLocation!) > 40000.0 {
            lastLocation = locationManager.location
            bufferedTrails.removeAll()
            for trail in allTrails {
                let trailLocation = CLLocation(latitude: trail.entrance.latitude, longitude: trail.entrance.longitude)
                let distanceInMeters = locationManager.location?.distance(from: trailLocation)
                if (distanceInMeters! / 1609.34) <= 125.0 {
                    bufferedTrails.append(trail)
                }
            }
        }
        nearbyTrails.removeAll()
        for trail in bufferedTrails {
            let trailLocation = CLLocation(latitude: trail.entrance.latitude, longitude: trail.entrance.longitude)
            let distanceInMeters = locationManager.location?.distance(from: trailLocation)
            if (distanceInMeters! / 1609.34) <= Double(currentDistance) {
                nearbyTrails.append(trail)
            }
        }
        if sortByDistance {
            nearbyTrails.sort{ (loc1, loc2) -> Bool in
                myDistanceSortFunc(location1: loc1, location2: loc2)
            }
        }
        else {
            nearbyTrails.sort{ (loc1, loc2) -> Bool in
                myDifficultySortFunc(trail1: loc1, trail2: loc2)
            }
        }
        tableChildVC?.nearbyTrails = self.nearbyTrails
        tableChildVC?.viewDidLoad()
        mapChildVC?.mapView.removeAnnotations((mapChildVC?.nearbyTrailAnnotations)!)
        mapChildVC?.nearbyTrailAnnotations.removeAll()
        for trail in nearbyTrails {
            let nextAnnotation = TrailAnnotation(coordinate: trail.entrance)
            mapChildVC?.nearbyTrailAnnotations.append(nextAnnotation)
        }
        mapChildVC?.currentDistance = self.currentDistance
        mapChildVC?.viewDidLoad()
        resultsLabel.text = "\(nearbyTrails.count) RESULTS"
    }*/
    
    @IBAction func sortButtonPressed(_ sender: UIButton) {
        if sortByDistance {
            sortByDistance = false
            let alert = UIAlertController(title: "Sorting By Difficulty", message: "Trails are now being sorted by difficulty. Press filter button again to sort by distance.", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default) { (alert: UIAlertAction!) -> Void in
                NSLog("You pressed button OK")
            }
            
            alert.addAction(defaultAction)
            
            present(alert, animated: true, completion:nil)
        }
        else {
            sortByDistance = true
            let alert = UIAlertController(title: "Sorting By Distance", message: "Trails are now being sorted by distance. Press filter button again to sort by difficulty.", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default) { (alert: UIAlertAction!) -> Void in
                NSLog("You pressed button OK")
            }
            
            alert.addAction(defaultAction)
            
            present(alert, animated: true, completion:nil)
        }
        if sortByDistance {
            nearbyTrails.sort{ (loc1, loc2) -> Bool in
                myDistanceSortFunc(location1: loc1, location2: loc2)
            }
        }
        else {
            nearbyTrails.sort{ (loc1, loc2) -> Bool in
                myDifficultySortFunc(trail1: loc1, trail2: loc2)
            }
        }
        tableChildVC?.nearbyTrails = self.nearbyTrails
        tableChildVC?.viewDidLoad()
    }
    
    @IBAction func distanceChanged(_ sender: UISegmentedControl) {
        switch segmentOutlet.selectedSegmentIndex {
        case 0:
            currentDistance = 10
        case 1:
            currentDistance = 50
        case 2:
            currentDistance = 100
        default:
            currentDistance = 10
        }
        //getNearbyTrails()
        getTrailsFromAPI()
    }
    
    func myDistanceSortFunc(location1: BikeTrail, location2: BikeTrail) -> Bool {
        let distance1 = CLLocation(latitude: location1.entrance.latitude, longitude: location1.entrance.longitude).distance(from: locationManager.location!)
        let distance2 = CLLocation(latitude: location2.entrance.latitude, longitude: location2.entrance.longitude).distance(from: locationManager.location!)
        if distance1 < distance2 {
            return true
        }
        else {
            return false
        }
    }
    
    func myDifficultySortFunc(trail1: BikeTrail, trail2: BikeTrail) -> Bool {
        let diff1 = trail1.difficulty
        let diff2 = trail2.difficulty
        if diff1 == "White" {
            return true
        }
        else if diff1 == "Double Black Diamond" {
            return false
        }
        else if diff1 == "Green" {
            if diff2 == "White" {
                return false
            }
            else {
                return true
            }
        }
        else if diff1 == "Blue" {
            if diff2 == "White" || diff2 == "Green" {
                return false
            }
            else {
                return true
            }
        }
        else if diff1 == "Black Diamond" {
            if diff2 == "Double Black Diamond" {
                return true
            }
            else {
                return false
            }
        }
        else {
            return true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "mapEmbedSegue" {
            mapChildVC = segue.destination as? DiscoverMapViewController
        }
        if segue.identifier == "tableViewEmbedSegue" {
            tableChildVC = segue.destination as? DiscoverTableViewController
            tableChildVC?.previousVC = self
        }
        if segue.identifier == "trailDetailSegue" {
            let destVC = segue.destination as? TrailDetailTableViewController
            destVC?.selectedTrail = self.selectedTrail!
        }
    }
    
}

