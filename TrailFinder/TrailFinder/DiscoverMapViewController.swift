//
//  DiscoverMapViewController.swift
//  TrailFinder
//
//  Created by Matthew Brown on 12/4/17.
//  Copyright © 2017 Matthew Brown. All rights reserved.
//

import UIKit
import CoreLocation
import Mapbox
import Firebase

class DiscoverMapViewController: UIViewController, MGLMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet var mapView: MyMapView!
    
    var currentDistance: Int = 10
    var nearbyTrailAnnotations = [TrailAnnotation]()
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        print(currentDistance)
        configureLocationManager()
        mapView.delegate = self
        switch currentDistance {
        case 10:
            mapView.setCenter((locationManager.location?.coordinate)!, zoomLevel: 9, animated: false)
        case 50:
            mapView.setCenter((locationManager.location?.coordinate)!, zoomLevel: 7, animated: false)
        default:
            mapView.setCenter((locationManager.location?.coordinate)!, zoomLevel: 6, animated: false)
        }
        mapView.addAnnotations(nearbyTrailAnnotations)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func mapViewDidFinishLoadingMap(_ mapView: MGLMapView) {
        if currentDistance == 10 {
            mapView.setCenter((locationManager.location?.coordinate)!, zoomLevel: 9, animated: false)
        }
        else if currentDistance == 50 {
            mapView.setCenter((locationManager.location?.coordinate)!, zoomLevel: 7, animated: false)
        }
        else {
            mapView.setCenter((locationManager.location?.coordinate)!, zoomLevel: 6, animated: false)
        }
        print("Finished Loading Map")
    }
    
    func configureLocationManager() {
        CLLocationManager.locationServicesEnabled()
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = 1.0
        locationManager.distanceFilter = 100.0
        locationManager.startUpdatingLocation()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
