//
//  HazardViewController.swift
//  TrailFinder
//
//  Created by Matthew Brown on 12/2/17.
//  Copyright © 2017 Matthew Brown. All rights reserved.
//

import UIKit
import CoreLocation
import Firebase
import Mapbox

class HazardViewController: UIViewController, MGLMapViewDelegate {

    @IBOutlet weak var mapView: MyMapView!
    @IBOutlet weak var sliderLabel: UILabel!
    @IBOutlet weak var timeSlider: UISlider!
    @IBOutlet weak var textField: UITextField!
    
    var hazardLocation: CLLocationCoordinate2D?
    var newHazard: TrailHazard?
    let ref = AppDelegate.shared().ref
    let apiString = "http://127.0.0.1:3000/Hazards"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        mapView.delegate = self
        updateNumLabel()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateNumLabel(){
        switch timeSlider.value {
        case 0..<1:
            sliderLabel.text = "1 Hour"
        case 1..<2:
            sliderLabel.text = "2 Hours"
        case 2..<3:
            sliderLabel.text = "3 Hours"
        case 3..<4:
            sliderLabel.text = "4 Hours"
        case 4..<5:
            sliderLabel.text = "5 Hours"
        case 5..<6:
            sliderLabel.text = "6 Hours"
        case 6..<7:
            sliderLabel.text = "7 Hours"
        case 7..<8:
            sliderLabel.text = "8 Hours"
        default:
            sliderLabel.text = "All Day"
        }
    }
    
    func mapViewDidFinishLoadingMap(_ mapView: MGLMapView) {
        mapView.setCenter(hazardLocation!, animated: false)
        let hazardAnnotation = MGLPointAnnotation()
        hazardAnnotation.coordinate = hazardLocation!
        mapView.addAnnotation(hazardAnnotation)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    @IBAction func sliderChanged(_ sender: UISlider) {
        updateNumLabel()
    }
    
    @IBAction func createHazard(_ sender: UIBarButtonItem) {
        var numOfHours = 24
        if timeSlider.value <= 8 {
            numOfHours = Int(timeSlider.value) + 1
        }
        let calendar = Calendar.current
        let startDate = Date()
        let futureDate = calendar.date(byAdding: .hour, value: numOfHours, to: Date())
        print("futureDate: \(String(describing: futureDate))")
        self.newHazard = TrailHazard(description: textField.text!, location: hazardLocation!, startTime: startDate, endTime: futureDate!)
        /*let hazardRef = ref?.child("Hazards").childByAutoId()
        hazardRef?.child("description").setValue(self.newHazard?.title)
        hazardRef?.child("start").setValue("\(self.newHazard!.startTime)")
        hazardRef?.child("end").setValue("\(self.newHazard!.endTime)")
        hazardRef?.child("lat").setValue(self.newHazard?.coordinate.latitude)
        hazardRef?.child("lon").setValue(self.newHazard?.coordinate.longitude)
        print("Hazard Saved")*/
        
        let postURL = URL(string: apiString)!
        var postRequest = URLRequest(url: postURL, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 60.0)
        postRequest.httpMethod = "POST"
        postRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        postRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        
        let parameters: [String: Any] = ["message": textField.text!, "lat": (hazardLocation?.latitude)!, "lon": (hazardLocation?.longitude)!, "hours": numOfHours]
        do {
            let jsonParams = try JSONSerialization.data(withJSONObject: parameters, options: [])
            postRequest.httpBody = jsonParams
        } catch { print("Error: unable to add parameters to POST request.")}
        
        URLSession.shared.dataTask(with: postRequest, completionHandler: { (data, response, error) -> Void in
            let stack = self.navigationController?.viewControllers
            let controller = stack?[0] as! MyLocationViewController
            controller.viewDidLoad()
            if error != nil { print("POST Request: Communication error: \(error!)") }
            if data != nil {
                do {
                    let resultObject = try JSONSerialization.jsonObject(with: data!, options: [])
                    DispatchQueue.main.async(execute: {
                        print("Results from POST https://httpbin.org/post :\n\(resultObject)")
                    })
                } catch {
                    DispatchQueue.main.async(execute: {
                        print("Unable to parse JSON response")
                    })
                }
            } else {
                DispatchQueue.main.async(execute: {
                    print("Received empty response.")
                })
            }
        }).resume()
        
        let stack = self.navigationController?.viewControllers
        print("count: \(stack!.count)")
        if (stack!.count > 1) {
            let previousController = stack![(stack?.count)!-2] as! MyLocationViewController
            previousController.viewDidLoad()
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cancelHazard(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
