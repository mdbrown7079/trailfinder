//
//  AppDelegate.swift
//  TrailFinder
//
//  Created by Matthew Brown on 12/2/17.
//  Copyright © 2017 Matthew Brown. All rights reserved.
//

import UIKit
import Firebase
import CoreLocation
import GoogleSignIn

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {

    var window: UIWindow?
    var ref: DatabaseReference!
    var storageRef: StorageReference!
    var savedTrailIds = Set<String>()
    var allTrails = [BikeTrail]()
    var favoriteTrails = [BikeTrail]()
    //var discoverVC: DiscoverViewController?

    static func shared() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
        if Auth.auth().currentUser != nil && CLLocationManager.locationServicesEnabled() {
            configureStorage()
            ref = Database.database().reference()
            /*ref.child("Saved").child((Auth.auth().currentUser?.uid)!).observeSingleEvent(of: .value, with: ({ (snapshot) in
                let value = snapshot.value as? [String: Any]
                if value == nil {
                    return
                }
                for key in (value?.keys)! {
                    self.savedTrailIds.insert(key)
                }
            }))*/
        }
        //getAllTrails()
        return true
    }
    
    func application(_ application: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any])
        -> Bool {
            return GIDSignIn.sharedInstance().handle(url,
                                                     sourceApplication:options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                     annotation: [:])
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        if let error = error {
            print("Error \(error)")
            return
        }
        
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        Auth.auth().signIn(with: credential) { (user, error) in
            if let error = error {
                print("Error \(error)")
                return
            }
        }
        self.ref = Database.database().reference()
    }
    
    func configureStorage() {
        storageRef = Storage.storage().reference()
    }
    
    /*func getAllTrails() {
        ref?.child("Trails").observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? [String: Any]
            for key in (value?.keys)! {
                var trail = value![key] as! [String:Any]
                self.allTrails.append(BikeTrail(name: trail["name"] as! String, park: trail["park"] as! String, city: trail["city"] as! String, state: trail["state"] as! String, difficulty: trail["difficulty"] as! String ,type: trail["type"] as! String, distance: trail["distance"] as! Double, ascent: trail["ascent"] as! Double, descent: trail["descent"] as! Double, high: trail["high"] as! Double, low: trail["low"] as! Double, minGrade: trail["minGrade"] as! Double, maxGrade: trail["maxGrade"] as! Double, entrance: CLLocationCoordinate2D(latitude: trail["lat"] as! Double, longitude: trail["lon"] as! Double), uniqueKey: key))
            }
            /*if self.discoverVC != nil {
                self.discoverVC?.allTrails = self.allTrails
                self.discoverVC?.viewDidLoad()
                self.discoverVC?.getNearbyTrails()
                self.discoverVC?.mapChildVC?.viewDidLoad()
                self.discoverVC?.tableChildVC?.tableView.reloadData()
            }*/
            //print(self.allTrails.count)
            //self.getNearbyTrails()
            //self.mapChildVC?.viewDidLoad()
            //self.tableChildVC?.viewDidLoad()
        }) { (error) in
            print(error.localizedDescription)
        }
    }*/

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

