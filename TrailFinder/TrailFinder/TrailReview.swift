//
//  TrailReview.swift
//  TrailFinder
//
//  Created by Matthew Brown on 12/5/17.
//  Copyright © 2017 Matthew Brown. All rights reserved.
//

import Foundation

class TrailReview {
    var userName: String
    var userID: String
    var text: String
    var rating: Int
    var date: Date
    
    init(userName: String, userID: String, text: String, rating: Int, date: Date) {
        self.userName = userName
        self.userID = userID
        self.text = text
        self.rating = rating
        self.date = date
    }
}
