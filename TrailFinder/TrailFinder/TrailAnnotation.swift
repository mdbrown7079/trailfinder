//
//  TrailAnnotation.swift
//  TrailFinder
//
//  Created by Matthew Brown on 12/4/17.
//  Copyright © 2017 Matthew Brown. All rights reserved.
//

import Foundation
import CoreLocation
import Mapbox

class TrailAnnotation: NSObject, MGLAnnotation {
    var coordinate: CLLocationCoordinate2D
    
    init(coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
    }
}
