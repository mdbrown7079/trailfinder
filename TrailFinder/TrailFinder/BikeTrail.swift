//
//  BikeTrail.swift
//  TrailFinder
//
//  Created by Matthew Brown on 12/3/17.
//  Copyright © 2017 Matthew Brown. All rights reserved.
//

import Foundation
import CoreLocation

class BikeTrail: Equatable {
    
    var name: String
    var park: String
    var city: String
    var state: String
    var difficulty: String
    var type: String
    var distance: Double
    var ascent: Double
    var descent: Double
    var high: Double
    var low: Double
    var minGrade: Double
    var maxGrade: Double
    var entrance: CLLocationCoordinate2D
    var uniqueKey: String
    
    init(name: String, park: String, city: String, state: String, difficulty: String, type: String, distance: Double, ascent: Double, descent: Double, high: Double, low: Double, minGrade: Double, maxGrade: Double, entrance: CLLocationCoordinate2D, uniqueKey: String) {
        self.name = name
        self.park = park
        self.city = city
        self.state = state
        self.difficulty = difficulty
        self.type = type
        self.distance = distance
        self.ascent = ascent
        self.descent = descent
        self.high = high
        self.low = low
        self.minGrade = minGrade
        self.maxGrade = maxGrade
        self.entrance = entrance
        self.uniqueKey = uniqueKey
    }
}

func ==(lhs: BikeTrail, rhs: BikeTrail) -> Bool {
    return lhs.uniqueKey == rhs.uniqueKey
}
