//
//  FavoritesCollectionViewCell.swift
//  TrailFinder
//
//  Created by Matthew Brown on 12/6/17.
//  Copyright © 2017 Matthew Brown. All rights reserved.
//

import UIKit

class FavoritesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    @IBAction func deleteButtonPressed(_ sender: UIButton) {
    }
}
