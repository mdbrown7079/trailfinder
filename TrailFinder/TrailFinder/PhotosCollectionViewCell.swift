//
//  PhotosCollectionViewCell.swift
//  TrailFinder
//
//  Created by Matthew Brown on 12/7/17.
//  Copyright © 2017 Matthew Brown. All rights reserved.
//

import UIKit

class PhotosCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
}
