//
//  FavoritesCollectionViewController.swift
//  TrailFinder
//
//  Created by Matthew Brown on 12/6/17.
//  Copyright © 2017 Matthew Brown. All rights reserved.
//

import UIKit
import Firebase
import CoreLocation

private let reuseIdentifier = "Cell"

class FavoritesCollectionViewController: UICollectionViewController {
    
    var favoriteTrails = [BikeTrail]()
    var allTrails = [BikeTrail]()
    var savedTrailIds = Set<String>()
    var selectedTrail: BikeTrail?
    let ref = Database.database().reference()
    let locationManager = CLLocationManager()
    let apiString = "http://127.0.0.1:3000/Trails"

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        //self.collectionView!.register(FavoritesCollectionViewCell.self, forCellWithReuseIdentifier: "favoritesCell")
        getAllFavoriteTrails()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        let destVC = segue.destination as? TrailDetailTableViewController
        destVC?.selectedTrail = self.selectedTrail
        // Pass the selected object to the new view controller.
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return favoriteTrails.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "favoritesCell", for: indexPath) as! FavoritesCollectionViewCell
    
        // Configure the cell
        let currentTrail = favoriteTrails[indexPath.row]
        cell.nameLabel.text = "\(indexPath.row + 1). \(currentTrail.name)"
        cell.locationLabel.text = "\(currentTrail.city), CA"
        cell.distanceLabel.text = "\(currentTrail.distance)m, \(currentTrail.type)"
    
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedTrail = favoriteTrails[indexPath.row]
        performSegue(withIdentifier: "trailDetailSegue", sender: self)
    }
    
    func getFavoriteTrailids() {
        /*self.savedTrailIds.removeAll()
        ref.child("Saved").child((Auth.auth().currentUser?.uid)!).observeSingleEvent(of: .value, with: ({ (snapshot) in
            let value = snapshot.value as? [String: Any]
            if value == nil {
                return
            }
            for key in (value?.keys)! {
                self.savedTrailIds.insert(key)
            }
            print("saved trail ids: \(self.savedTrailIds.count)")
            self.getFavoriteTrails()
        }))*/
        let userID = Auth.auth().currentUser?.uid
        let getURL = URL(string: "http://127.0.0.1:3000/Favorites?userid=\(userID!)")!
        var getRequest = URLRequest(url: getURL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 30.0)
        getRequest.httpMethod = "GET"
        getRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        getRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        
        URLSession.shared.dataTask(with: getRequest, completionHandler: { (data, response, error) -> Void in
            if error != nil { print("GET Request: Communication error: \(error!)") }
            if data != nil {
                do {
                    let resultObject = try JSONSerialization.jsonObject(with: data!, options: [])
                    DispatchQueue.main.async(execute: {
                        print("Results from GET https://httpbin.org/get?bar=foo :\n\(resultObject)")
                        for item in (resultObject as! NSArray) {
                            let trail = (item as! NSDictionary)
                            print(trail["ID"] as! String)
                            
                            self.savedTrailIds.insert(trail["ID"] as! String)
                        }
                    })
                    
                } catch {
                    DispatchQueue.main.async(execute: {
                        print("Unable to parse JSON response")
                    })
                }
            } else {
                DispatchQueue.main.async(execute: {
                    print("Received empty response.")
                })
            }
        }).resume()
    }
    
    func getAllFavoriteTrails() {
        self.favoriteTrails.removeAll()
        let userID = Auth.auth().currentUser?.uid
        let getURL = URL(string: "http://127.0.0.1:3000/Favorites?userid=\(userID!)")!
        var getRequest = URLRequest(url: getURL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 30.0)
        getRequest.httpMethod = "GET"
        getRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        getRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        
        URLSession.shared.dataTask(with: getRequest, completionHandler: { (data, response, error) -> Void in
            if error != nil { print("GET Request: Communication error: \(error!)") }
            if data != nil {
                do {
                    let resultObject = try JSONSerialization.jsonObject(with: data!, options: [])
                    DispatchQueue.main.async(execute: {
                        print("Results from GET https://httpbin.org/get?bar=foo :\n\(resultObject)")
                        for item in (resultObject as! NSArray) {
                            let trail = (item as! NSDictionary)
                            self.favoriteTrails.append(BikeTrail(name: trail["Name"] as! String, park: trail["Park"] as! String, city: trail["City"] as! String, state: trail["State"] as! String, difficulty: trail["Difficulty"] as! String ,type: trail["Type"] as! String, distance: trail["Distance"] as! Double, ascent: trail["Ascent"] as! Double, descent: trail["Descent"] as! Double, high: trail["High"] as! Double, low: trail["Low"] as! Double, minGrade: trail["Mingrade"] as! Double, maxGrade: trail["Maxgrade"] as! Double, entrance: CLLocationCoordinate2D(latitude: trail["Lat"] as! Double, longitude: trail["Lon"] as! Double), uniqueKey: trail["ID"] as! String))
                            
                            //self.savedTrailIds.insert(trail["ID"] as! String)
                        }
                        self.collectionView?.reloadData()
                    })
                    
                } catch {
                    DispatchQueue.main.async(execute: {
                        print("Unable to parse JSON response")
                    })
                }
            } else {
                DispatchQueue.main.async(execute: {
                    print("Received empty response.")
                })
            }
        }).resume()
        self.collectionView?.reloadData()
    }
    

    
    /*func getFavoriteTrails() {
        //savedTrailIds = AppDelegate.shared().savedTrailIds
        favoriteTrails.removeAll()
        for trail in allTrails {
            if savedTrailIds.contains(trail.uniqueKey) {
                favoriteTrails.append(trail)
                print(trail.uniqueKey)
            }
        }
        print("number of saved trails: \(savedTrailIds.count)")
        self.collectionView?.reloadData()
    }*/
    

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
