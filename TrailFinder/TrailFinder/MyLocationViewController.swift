//
//  FirstViewController.swift
//  TrailFinder
//
//  Created by Matthew Brown on 12/2/17.
//  Copyright © 2017 Matthew Brown. All rights reserved.
//

import UIKit
import CoreLocation
import Mapbox
import Firebase
import GoogleSignIn

class MyLocationViewController: UIViewController, MGLMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet var mapView: MyMapView!
    
    let locationManager = CLLocationManager()
    let ref = Database.database().reference()
    let hazardAPIString = "http://127.0.0.1:3000/Hazards"
    var currentHazards: [TrailHazard] = []
    var tapCoordinate: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configureLocationManager()
        mapView.delegate = self
        mapView.userTrackingMode = .follow
        //addHazardAnnotations()
        getHazardsFromAPI()
        if GIDSignIn.sharedInstance().hasAuthInKeychain() {
            print("user is signed in")
        }
        else {
            print("user is not signed in")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func mapViewDidFinishLoadingMap(_ mapView: MGLMapView) {
        mapView.addAnnotations(currentHazards)
    }
    
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        return true
    }
    
    func configureLocationManager() {
        CLLocationManager.locationServicesEnabled()
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = 1.0
        locationManager.distanceFilter = 100.0
        locationManager.startUpdatingLocation()
    }

    @IBAction func moreButtonPressed(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let firstAction = UIAlertAction(title: "Report Hazard or Closure", style: .default) { (alert: UIAlertAction!) -> Void in
            NSLog("You pressed button one")
            self.performSegue(withIdentifier: "hazardSegue", sender: self)
        }
        let secondAction = UIAlertAction(title: "Sign Out", style: .destructive) { (alert: UIAlertAction!) -> Void in
            NSLog("You pressed button two")
            let firebaseAuth = Auth.auth()
            do {
                try firebaseAuth.signOut()
                GIDSignIn.sharedInstance().signOut()
                self.dismiss(animated: true, completion: nil)
            } catch let signOutError as NSError {
                print ("Error signing out: \(signOutError.localizedDescription)")
            }
        }
        
        let thirdAction = UIAlertAction(title: "Cancel", style: .cancel) { (alert: UIAlertAction!) -> Void in
            NSLog("Canceling")
        }
        
        alert.addAction(firstAction)
        alert.addAction(secondAction)
        alert.addAction(thirdAction)
        present(alert, animated: true, completion:nil)
    }
    
    @IBAction func longPressGesture(_ sender: UILongPressGestureRecognizer) {
        if sender.state != .began {
            return
        }
        tapCoordinate = mapView.convert(sender.location(in: mapView), toCoordinateFrom: mapView)
        print("You tapped at: \(tapCoordinate.latitude), \(tapCoordinate.longitude)")
        performSegue(withIdentifier: "longPressSegue", sender: self)
    }
    
    /*func addHazardAnnotations() {
        ref.child("Hazards").observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? [String: Any]
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ"
            if value == nil {
                return
            }
            for key in (value?.keys)! {
                let hazard = value![key] as? [String: Any]
                let currentHazard = TrailHazard(description: hazard!["description"] as! String, location: CLLocationCoordinate2D(latitude: hazard!["lat"] as! Double, longitude: hazard!["lon"] as! Double), startTime: dateFormatter.date(from: hazard!["start"] as! String)!, endTime: dateFormatter.date(from: hazard!["end"] as! String)!)
                //print(currentHazard.description)
                let currentDate = Date()
                print("current date: \(currentDate)")
                print("hazard date: \(currentHazard.endTime)")
                if currentDate < currentHazard.endTime {
                    self.currentHazards.append(currentHazard)
                }
                else if Date() > currentHazard.endTime {
                    self.ref.child("Hazards").child(key).removeValue()
                }
            }
            self.mapView.addAnnotations(self.currentHazards)
        }) { (error) in
            print(error.localizedDescription)
        }
    }*/
    
    func getHazardsFromAPI() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ"
        let apiURL = URL(string: hazardAPIString)!
        var getRequest = URLRequest(url: apiURL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 30.0)
        getRequest.httpMethod = "GET"
        getRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        getRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        
        URLSession.shared.dataTask(with: getRequest, completionHandler: { (data, response, error) -> Void in
            if error != nil { print("GET Request: Communication error: \(error!)") }
            if data != nil {
                do {
                    let resultObject = try JSONSerialization.jsonObject(with: data!, options: [])
                    DispatchQueue.main.async(execute: {
                        //print("Results from GET https://httpbin.org/get?bar=foo :\n\(resultObject)")
                        for item in (resultObject as! NSArray) {
                            let hazard = (item as! NSDictionary)
                            let currentHazard = TrailHazard(description: hazard["Message"] as! String, location: CLLocationCoordinate2D(latitude: hazard["Lat"] as! Double, longitude: hazard["Lon"] as! Double), startTime: Date(), endTime: Date())
                            self.currentHazards.append(currentHazard)
                            print(hazard["Message"] as! String)
                        }
                        self.mapView.addAnnotations(self.currentHazards)
                    })
                } catch {
                    DispatchQueue.main.async(execute: {
                        print("Unable to parse JSON response")
                    })
                }
            } else {
                DispatchQueue.main.async(execute: {
                    print("Received empty response.")
                })
            }
        }).resume()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destVC = segue.destination as? HazardViewController
        if segue.identifier == "hazardSegue" {
            destVC?.hazardLocation = self.mapView.userLocation?.coordinate
        }
        if segue.identifier == "longPressSegue" {
            destVC?.hazardLocation = self.tapCoordinate
        }
    }
    
}

